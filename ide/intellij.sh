#!/usr/bin/env bash

inotifyLimit() {

	INOTIFY_LIMIT_FILE=/etc/sysctl.d/docker-tools.conf

	if [ ! -e $INOTIFY_LIMIT_FILE ]; then
		e "System" "Create Inotify Limit file... (need sudo)"

		sudo touch $INOTIFY_LIMIT_FILE
		sudo chown $USER $INOTIFY_LIMIT_FILE

		{
			echo "fs.inotify.max_user_watches = 524288"
		} > $INOTIFY_LIMIT_FILE

		sudo sysctl -p --system
	fi

	{
		echo "fs.inotify.max_user_watches = 524288"
	} > $INOTIFY_LIMIT_FILE

	e System "Add inotify file to system, run 'sudo sysctl -p --system' to reload it."
}
