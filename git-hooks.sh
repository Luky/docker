#!/usr/bin/env bash



# -----------------------------------------------------------------------------------------------------
# PRE COMMIT's
# -----------------------------------------------------------------------------------------------------

{
	echo "#!/usr/bin/env bash"
	echo "$DIR/docker.sh pre-commit"
} > $DIR/.git/hooks/pre-commit

chmod +x $DIR/.git/hooks/pre-commit

# -----------------------------------------------------------------------------------------------------
# POST COMMIT's
# -----------------------------------------------------------------------------------------------------

{
	echo "#!/usr/bin/env bash"
	echo "$DIR/docker.sh post-commit"
} > $DIR/.git/hooks/post-commit

chmod +x $DIR/.git/hooks/post-commit

# -----------------------------------------------------------------------------------------------------
# PRE PUSH's
# -----------------------------------------------------------------------------------------------------

{
	echo "#!/usr/bin/env bash"
	echo "$DIR/docker.sh pre-push"
} > $DIR/.git/hooks/pre-push

chmod +x $DIR/.git/hooks/pre-push

# -----------------------------------------------------------------------------------------------------
# PRE PUSH's
# -----------------------------------------------------------------------------------------------------

{
	echo "#!/usr/bin/env bash"
	echo "$DIR/docker.sh prepare-commit-msg"
} > $DIR/.git/hooks/prepare-commit-msg

chmod +x $DIR/.git/hooks/prepare-commit-msg

