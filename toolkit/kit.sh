#!/usr/bin/env bash

createDebugModeFile () {
	if [ $2 = "1" ]; then
		echo "DEBUG mode ENABLED"
		{
			echo "<?php declare(strict_types = 1);"
			echo "return true;"
		} > $1

	else
		echo "DEBUG mode DISABLED"
		{
			echo "<?php declare(strict_types = 1);"
			echo "return false;"
		} > $1
	fi
}

createOrClearDirectory () {
	DIR=`pwd`
	if [ -e $1 ]
	then
		rm -rf $1/*
	else
		mkdir $1 -m 775
	fi

	cd $1 && umask 002
	cd $DIR
}
