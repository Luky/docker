#!/usr/bin/env bash

echo $*


NAME=$( git branch | grep '*' | sed 's/* //' )

echo "Post-commit hook"

if [[ $NAME == *"release/"* ]]; then
	echo "Automerge develop branch"
	{
		echo "#!/usr/bin/env bash"
		echo "DCKSHL_VERSION="
		echo $NAME | sed 's/[^0-9]*\([0-9\.]\+\).*/\1/'
	} > _version.sh

	cat _version.sh

fi

