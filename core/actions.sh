#!/usr/bin/env bash


dockerExecPhp() {
	CONTAINER_NAME=$1
	shift

	docker exec -it $CONTAINER_NAME php "$@"
}

dockerExecComposer() {
	CONTAINER_NAME=$1
	shift

	docker exec -it $CONTAINER_NAME composer "$@"
}
dockerExecTest() {
	CONTAINER_NAME=$1
	shift

	docker exec -i $CONTAINER_NAME vendor/bin/codecept run "$@"
}

dockerExecLint() {
	CONTAINER_NAME=$1
	shift

	docker exec -i $CONTAINER_NAME vendor/bin/parallel-lint --exclude vendor "$@"
}

dockerExecStan() {
	CONTAINER_NAME=$1
	shift

	if [ $PHPSTAN_LEVEL ]; then
	docker exec -i $CONTAINER_NAME vendor/bin/phpstan -l $PHPSTAN_LEVEL "$@"
	else
		docker exec -i $CONTAINER_NAME vendor/bin/phpstan "$@"
	fi
}

#
# Actions
#

actionPhp() {
	if [[ $ACTION == "php" ]]; then
		set -e
		dockerExecPhp "$@"
	exit 0
	fi
}

actionComposer() {
	if [[ $ACTION == "composer" ]]; then
		set -e
		dockerExecComposer "$@"
	exit 0
	fi
}


actionTest() {
	if [[ $ACTION == "tests" ]] || [[ $ACTION == "test" ]]; then
		set -e
		dockerExecTest "$@"
	exit 0
	fi
}



actionLint() {
	if [[ $ACTION == "lint" ]]; then
		set -e
		dockerExecLint "$@"
	exit 0
	fi
}


actionPhpstan() {
	if [[ $ACTION == "phpstan" ]] || [[ $ACTION == "stan" ]]; then
		set -e
		dockerExecStan "$@"
	exit 0
	fi
}
