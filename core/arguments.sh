#!/usr/bin/env bash

OPT_FORCE=0
OPT_BUILD=0
OPT_VERBOSE=0

OPT_HOOKS=1
OPT_ALIASES=1
OPT_ALLOW_ROOT=0

for var in "$@"
do
	case "$var" in
		-f|--force)
			OPT_FORCE=1
			;;
		-v|--verbose)
			OPT_VERBOSE=1
			;;
		-b|--build)
			OPT_BUILD=1
			;;
		--no-aliases)
			OPT_ALIASES=0
			;;
		--no-hooks)
			OPT_HOOKS=0
			;;
		--allow-root)
			OPT_ALLOW_ROOT=1
			;;
		*)
			;;
	esac
done
