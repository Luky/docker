#!/usr/bin/env bash

# These functions support functionality of docker.sh at project root


DCKSHL_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source $DCKSHL_DIR/_version.sh

# Imports
source $DCKSHL_DIR/core/colors.sh
source $DCKSHL_DIR/core/helpers.sh
source $DCKSHL_DIR/core/actions.sh

source $DCKSHL_DIR/ide/intellij.sh

DCKSHL_LOGO="[${CLR_CYAN}Docker.sh${CLR_END}]"

LOG_FILE=$DIR/docker.sh.log 2>&1

RELOG=0

# -----------------------------------------------------------------------------------------------------
# Imports
# -----------------------------------------------------------------------------------------------------

source $DCKSHL_DIR/injector.sh

source $DCKSHL_DIR/core/arguments.sh

source $DCKSHL_DIR/docker-shell.sh


# -----------------------------------------------------------------------------------------------------
# Helpers
# -----------------------------------------------------------------------------------------------------


groupPropagation() {
	if id | grep &>/dev/null '\'$1'\b'; then
		e "INIT" "${CLR_PURPLE}$1${CLR_END} was propagated in current session"
	else
		e "INIT" "${CLR_PURPLE}$1${CLR_END} missing in current session, relog your account or use 'sudo su - $USER' terminal"
	fi
}


## Stop All Containers in DOCKER
## - With Side Effect
stopWholeContainer() {
	# List all running containers and stop them

	if [ $OPT_FORCE -eq 1 ]
	then
		echo "Kill containers:"
		docker ps --format {{.Names}} | xargs docker kill
	else
		echo "Stop containers:"
		docker ps --format {{.Names}} | xargs docker stop
	fi
}



printHosts() {
	e "HOSTS" "Append your /etc/hosts with these lines"

	echo ""
	echo "# --------------------------------------------------------------"
	echo "# Docker's hosts"
	echo "# --------------------------------------------------------------"
	echo "172.21.0.20 dmpm.docker              #dmpm-docker#"
	echo "172.21.1.20 dmpm-tv.docker           #dmpm-tv-docker#"
	echo "172.21.1.20 api.dmpm-tv.docker       #api-dmpm-tv-docker#"
	echo "172.21.2.20 dmpm-delivery.docker     #dmpm-delivery-docker#"
	echo "172.21.3.50 webgarden.docker         #webgarden-docker#"
	echo "172.21.4.20 adtrack.docker           #adtrack-docker#"
	echo "172.21.5.20 aym.docker               #aym-docker#"
	echo "# --------------------------------------------------------------"
	echo "# Docker's hosts END"
	echo "# --------------------------------------------------------------"
	echo ""
	e "HOSTS" ""
}

dockerCompose() {
	docker-compose -f $DOCKER_COMPOSE_FILE $*
}
# -----------------------------------------------------------------------------------------------------
# Docker Proxy
# -----------------------------------------------------------------------------------------------------

dockerBuild() {
	e "Build" "Container App: $1 Name: $2"

	APP=$1
	NAME=$2
	VERBOSE=$3

	shift
	shift
	shift

	if [ $VERBOSE = 1 ]; then
		docker build $DIR/_docker/$APP --build-arg DOCKER_UID=$UID --tag $NAME $*
	else
		docker build $DIR/_docker/$APP --build-arg DOCKER_UID=$UID -q --tag $NAME $* >> /dev/null
	fi
}

dockerShip() {
	e "Ship" "Ship container $1 to docker.io HUB"

	NAME=$1
	VERBOSE=$2

	shift
	shift

	if [ $VERBOSE = 1 ]; then
		docker build $DIR/_docker/ship --tag $NAME:gitlab
	else
		docker build $DIR/_docker/ship -q --tag $NAME:gitlab >> /dev/null
	fi

	docker push $NAME:gitlab
}

dockerComposeUp() {
	e "System" "Start composition"
	dockerCompose up -d
}

dockerComposeStop() {
	if [ $1 -eq 1 ]
	then
		e "System" "Kill running composition"
		dockerCompose kill
	else
		e "System" "Stop running composition"
		dockerCompose stop
	fi
}

dockerComposeBuild() {
    dockerCompose build
}

dockerLogs() {
	dockerCompose logs -f --tail="$1"
}

dockerIntranet() {
	CONTAINER=$1; shift
	docker network disconnect intranet $CONTAINER || true
	docker network connect intranet $CONTAINER $*
}

dockerAddGroup() {
	e "INIT" "${DOCKER_GROUP} group"
	if groups $USER | grep &>/dev/null '\docker\b'; then
		e "INIT" "Already in $DOCKER_GROUP group"
	else
		e "INIT" "Add user to $DOCKER_GROUP group"
		sudo addgroup docker >> $LOG_FILE 2>&1
		sudo adduser $USER docker >> $LOG_FILE 2>&1 \
		&& e "INIT" "Added to $DOCKER_GROUP group, now relog your account or use 'sudo su - $USER'" \
		|| ( e "INIT" "Something went wrong" && exit 1 )
	fi
}

dockerInstall() {
	# Docker install from
	# https://docs.docker.com/engine/installation/linux/ubuntu/#install-using-the-repository
	sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

   	sudo apt-get update
   	sudo apt-get install docker-ce -y

	# Docker compose install from
	# https://docs.docker.com/compose/install/
	sudo curl -L "https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
	sudo chmod +x /usr/local/bin/docker-compose
	dockerAddGroup


}

# -----------------------------------------------------------------------------------------------------
# Actions
# -----------------------------------------------------------------------------------------------------

actionNetwork() {
	e "System" "Create Intranet network"

	stopWholeContainer
	docker network prune
	docker network create --attachable --subnet 172.21.0.0/16 --gateway 172.21.0.1 intranet

	e "System" "Intranet network created."
}

actions() {

	if [[ $1 == "version" ]]; then
		e "Versions" "Required: $DOCKER_TOOLS_VERSION"
		e "Versions" "Installed: $DCKSHL_VERSION"

		exit 0
	fi

	if [[ $1 == "install" ]]; then
		dockerInstall
		exit 0
	fi

	if [[ $1 == "init" ]]; then
		init

		exit 0
	fi

	if [[ $1 == "network" ]]; then
		actionNetwork

		exit 0
	fi

	if [[ $1 == "stop" ]]; then
		dockerComposeStop 0

		exit 0
	fi

	if [[ $1 == "kill" ]]; then
		dockerComposeStop 1

		exit 0
	fi

	if [[ $1 == "stopall" ]]; then
		stopWholeContainer

		exit 0
	fi

	if [[ $1 == "host" ]] || [[ $1 == "hosts" ]]; then
		printHosts
		exit 0
	fi

	if [[ $1 == "log" ]] || [[ $1 == "logs" ]]; then
		dockerLogs $2
		exit 0
	fi

	if [[ $1 == "run" ]]; then
		createCron "*/15 * * * * docker pull webgarden/cs >> /var/log/syslog" "cron-webgarden-cs-pull"
	fi

	if [[ $1 == "run" ]] || [[ $1 == "stop" ]] || [[ $1 == "kill" ]] || [[ $1 == "stopall" ]]; then
		touchFile $DIR/.env.local
	fi
}

actionHooks() {
	if [[ $1 == "pre-commit" ]] || [[ $1 == "pre-push" ]]
	then
		exit 0
	fi
}

actionExec() {
	if [[ $1 == "exec" ]]
	then

		shift
		docker exec -it $*
		exit 0
	fi
}

# -----------------------------------------------------------------------------------------------------
# Init's
# -----------------------------------------------------------------------------------------------------
initAction() {

	if [ -e $DIR/.git ]; then
		if [ $OPT_HOOKS = "1" ]; then
			source $DCKSHL_DIR/git-hooks.sh
		else
			e "System" "Git hooks skiped by --no-hooks"
		fi
	fi

	if [ -z "$1" ]
	  then
		e "System" "No argument supplied [run, stop, stopall, network]"
		exit 1
	fi

	if [ $OPT_ALLOW_ROOT = "0" ]; then
		rootCheck
	else
		e "System" "Root check skipped by --allow-root"
	fi


	actions $*


}

createCron() {
	e "CRON" "Create command ($2)"
	crontab -l | fgrep -i -v "$2" | { cat; echo "$1" "#$2"; } | crontab -
}

createHost() {
	e "HOSTS" "Create hosts entry ($2)"

	e "HOSTS" "Some Tries"


	cat /etc/hosts | fgrep -i -v "#$2" | cat > /etc/hosts


	HOST_NAME_BACKUP=/home/$USER/hosts-backup-$(now)
	e "HOSTS" "Backup hosts file: $HOST_NAME_BACKUP"
	cat /etc/hosts
	cat /etc/hosts > $HOST_NAME_BACKUP

	cat /etc/hosts | fgrep -i -v "#$2" | cat > /etc/hosts



	e "HOSTS" "Current Hosts"
	cat /etc/hosts

	exit;

}

init() {
	DOCKER_GROUP=${CLR_PURPLE}"docker"${CLR_END}
	HOSTS_EDITORS_GROUP=${CLR_PURPLE}"hosts-editors"${CLR_END}

	e "INIT" "Init environment"
	e "INIT" "There are some steps to do"
	e "INIT" " - Add $USER to $DOCKER_GROUP group (grant access to docker.socket)"
	e "INIT" " - Add $USER to $HOSTS_EDITORS_GROUP group (grant access to append hosts file w/o sudo)"
	e "INIT" " - Create intranet network"


	hr "INIT"

	dockerAddGroup

	hr "INIT"


	e "INIT" "$HOSTS_EDITORS_GROUP group"
	if groups $USER | grep &>/dev/null '\hosts-editors\b'; then
		e "INIT" "Already in $HOSTS_EDITORS_GROUP group"
	else
		e "INIT" "Add user to $HOSTS_EDITORS_GROUP group"
		sudo addgroup hosts-editors >> $LOG_FILE 2>&1
		sudo chown :hosts-editors /etc/hosts >> $LOG_FILE 2>&1
		sudo chmod 664 /etc/hosts >> $LOG_FILE 2>&1

		sudo adduser $USER hosts-editors >> $LOG_FILE 2>&1 \
		&& e "INIT" "Added to $HOSTS_EDITORS_GROUP group, now relog your account or use 'sudo su - $USER'" \
		|| ( e "INIT" "Something went wrong" && exit 1 )
	fi

	hr "INIT"

	e "INIT" "Check group propagation"
	groupPropagation "docker"
	groupPropagation "hosts-editors"

	actionNetwork


	printHosts

}

dockerListCommands() {
	e "System" "  User Commands:"
	e "System" "  - run"
	e "System" "  - stop"
	e "System" "  - stopall"

	e "System" "  Environment Commands:"
	e "System" "  - init        Init system environment"
	e "System" "  - install	 	Install docker & docker compose"
	e "System" "  - network     Create Intranet network (stop all containers)"
	e "System" "  - hosts	    Print all hosts for docker"
	e "System" ""
}

dockerShellTerminate() {
	actionHooks $ACTION

	e "System" "Unknown argument '$ACTION', valid are these:"
	dockerListCommands
	exit 1
}

DOCKER_HUB_LOGIN=0
DOCKER_HUB_PASSWORD=0

if [ -e $DIR/config.sh ]; then
	source $DIR/config.sh
else
	if [ -e $DIR/.config.sh ]; then
		source $DIR/.config.sh
	fi
fi


